package oner;

import types.Attribute;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class OneR {

    HashMap<String, String> rules;
    ArrayList<String> classVal;

    public OneR() {
        rules = new HashMap<>();
        classVal = new ArrayList<>();
    }

    public HashMap<String, String> getRules() {
        return rules;
    }

    private Attribute getStrongestAttribute(HashMap<String, Collection<String>> dataset, String attributeName, ArrayList<Attribute> attributes) {
        classVal = (ArrayList<String>) dataset.get(attributeName);
        dataset.remove(attributeName);
        HashMap<String, HashMap<String, Integer>> statistic = new HashMap<>();

        Attribute strongestAttribute = null;

        for (Attribute attribute : attributes) { // идём по всем значениям аттрибута a
            int i = 0;
            if (dataset.get(attribute.getName()) == null) continue;
            for (String string : dataset.get(attribute.getName())) {
                if (!statistic.containsKey(string)) {
                    statistic.put(string, new HashMap<>());
                    classVal.forEach(b -> {
                        statistic.get(string).put(b, 0);
                    });
                    statistic.get(string).put(classVal.get(i), 1); // увеличили одно из значений целевого аттрибута на 1
                    i++;
                } else {
                    statistic.get(string).put(classVal.get(i), statistic.get(string).get(classVal.get(i)) + 1); // если есть такое значение, то получаем значение целевого аттрибута и увеличиваем его на 1
                    i++;
                }
            }
            statistic.keySet().forEach(r -> {
                final int[] max = {0};
                statistic.get(r).forEach((s, integer) -> {
                    if (statistic.get(r).get(s) > max[0]) {
                        rules.put(r, s);
                        max[0] = statistic.get(r).get(s);
                    }
                });
            });
            System.out.println("Attribute: " + attribute.getName() + " Incorrectly classified instances: " +
                    getAccuracy(rules, attribute, classVal, dataset));
            if (strongestAttribute == null) strongestAttribute = attribute;
            else {
                if (getAccuracy(rules, attribute, classVal, dataset) < getAccuracy(rules, strongestAttribute, classVal, dataset)) {
                    strongestAttribute = attribute;
                }
            }
        }
        return strongestAttribute;
    }

    public String getOneRForAttribute(HashMap<String, Collection<String>> dataset, String attributeName, ArrayList<Attribute> attributes) {
        Attribute strongestAttribute = getStrongestAttribute(dataset, attributeName, attributes);
        HashMap<String, String> rules = getRules();
        int positiveValue = 0;
        int i = 0;
        for (String s : dataset.get(strongestAttribute.getName())) {
            if (rules.get(s).equals(getClassVal().get(i))) {
                positiveValue++;
                i++;
            } else i++;
        }

        System.out.println("Strongest attribute is:" + strongestAttribute.getName());

        System.out.println("Rules:");
        strongestAttribute.getPossibleValues().forEach(a -> {
            System.out.println("If " + a + " Then " + rules.get(a).toString());
        });

        String result = "Correctly classified instances:        "
                + positiveValue + "     "
                + ((double) positiveValue / (double) dataset.get(strongestAttribute.getName()).size()) * 100 + "%" + "\n";
        result += "Incorrect classified instances:        "
                + (dataset.get(strongestAttribute.getName()).size() - positiveValue)
                + "      "
                + (100 - ((double) positiveValue / (double) dataset.get(strongestAttribute.getName()).size()) * 100 + "%");
        return result;
    }


    private int getAccuracy(HashMap<String, String> rules, Attribute attribute,
                            ArrayList<String> classVal, HashMap<String, Collection<String>> dataset) { // возвращает кол-во неправильно предугаданных меток целевого аттрибута

        int i = 0;
        int correctlyClassified = 0;
        for (String s : dataset.get(attribute.getName())) {
            if (rules.get(s).equals(classVal.get(i))) {
                correctlyClassified++;
                i++;
            } else {
                i++;
            }
        }
        return classVal.size() - correctlyClassified;
    }

    public ArrayList<String> getClassVal() {
        return classVal;
    }
}
