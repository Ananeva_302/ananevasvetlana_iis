import oner.OneR;
import reader.ARFFFileReader;


import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        File f = new File("C:\\Program Files\\Weka-3-8\\data\\weather.nominal.arff");
        ARFFFileReader reader = new ARFFFileReader();
        reader.readFile(f);
        reader.getAttributes().forEach(a -> {
            System.out.println(a.toString());
        });
        OneR oneR = new OneR();
        System.out.println("OneR result>> ");
        System.out.println(oneR.getOneRForAttribute(reader.getDataSet(), "play", reader.getAttributes()));
    }
}