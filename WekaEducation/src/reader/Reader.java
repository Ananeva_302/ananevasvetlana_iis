package reader;

import java.io.File;

import java.io.IOException;
public interface Reader {

    void readFile(File file)  throws IOException;


}
